const assert = require('assert');
const largestProductInSeries = require('../index.js');


describe('Problem8', function() {
  it('largest product made from 4 digits in bigNumber', function() {
    assert.equal(largestProductInSeries(4), 5832);
  });
  it('largest product made from 13 digits in bigNumber', function() {
    assert.equal(largestProductInSeries(13), 23514624000);
  });
});

